﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Block
{
    public abstract Color Color
    {
        get;
    }
}


public class Air : Block
{
    public override Color Color
    {
        get
        {
            throw new NotImplementedException();
        }
    }
}
