﻿using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(SoftwareAgentsGenerator), true)]
public class SoftwareAgentsEditor : GeneratorEditor
{
    
    private bool showBeachAgents = false;
    private bool enableBeachAgents = false;
    private bool showMountainAgents = false;
    private bool enableMountainAgents = false;
    private bool showCoastlineAgents = false;
    private bool showSmoothAgents = false;
    private bool enableSmoothAgents = false;


    private bool showDefaultInspector = false;

    //General property
    SerializedProperty generalSeed;
    SerializedProperty generalTokens;
    SerializedProperty generalTokensLimit;


    //Coastline agents property
    SerializedProperty coastlineRadius;
    SerializedProperty coastlineWaterLevel;
    SerializedProperty coastlineLandLevel;
    SerializedProperty coastlineStartPoint;
    SerializedProperty coastlineRemoveArtefacts;

    //Mountain agents property
    SerializedProperty mountainEnable;
    SerializedProperty mountainActiveAgents;
    SerializedProperty mountainBottomWidthMin;
    SerializedProperty mountainBottomWidthMax;
    SerializedProperty mountainTopWidthMin;
    SerializedProperty mountainTopWidthMax;
    SerializedProperty mountainHeightMin;
    SerializedProperty mountainHeightMax;
    SerializedProperty mountainStepBetweenChangeDirectionMin;
    SerializedProperty mountainStepBetweenChangeDirectionMax;

    //Beach agents property
    SerializedProperty beachStartHeight;
    SerializedProperty beachEndHeight;
    SerializedProperty beachHeightLimit;
    SerializedProperty beachWalkDistance;
    SerializedProperty beachWalkSize;
    SerializedProperty beachBrushRadius;
    SerializedProperty beachEnable;

    //Smoothing agents property
    SerializedProperty smoothIterations;
    SerializedProperty smoothReturning;
    SerializedProperty smoothEnable;

    private void OnEnable()
    {
        //General property
        generalSeed = serializedObject.FindProperty("generalSeed");
        generalTokens = serializedObject.FindProperty("generalTokens");
        generalTokensLimit = serializedObject.FindProperty("generalTokensLimit");

        //Coastline agents property
        coastlineRadius = serializedObject.FindProperty("coastlineRadius");
        coastlineWaterLevel = serializedObject.FindProperty("coastlineWaterLevel");
        coastlineLandLevel = serializedObject.FindProperty("coastlineLandLevel");
        coastlineStartPoint = serializedObject.FindProperty("coastlineStartPoint");
        coastlineRemoveArtefacts = serializedObject.FindProperty("coastlineRemoveArtefacts"); 

        //Mountain agents property
        mountainEnable = serializedObject.FindProperty("mountainAgentEnable");
        mountainActiveAgents = serializedObject.FindProperty("mountainAgentsCount");
        mountainBottomWidthMin = serializedObject.FindProperty("mountainBottomWidthMin");
        mountainBottomWidthMax = serializedObject.FindProperty("mountainBottomWidthMax");
        mountainTopWidthMin = serializedObject.FindProperty("mountainTopWidthMin");
        mountainTopWidthMax = serializedObject.FindProperty("mountainTopWidthMax");
        mountainHeightMin = serializedObject.FindProperty("mountainHeightMin");
        mountainHeightMax = serializedObject.FindProperty("mountainHeightMax");
        mountainStepBetweenChangeDirectionMin = serializedObject.FindProperty("mountainStepBetweenChangeDirectionMin");
        mountainStepBetweenChangeDirectionMax = serializedObject.FindProperty("mountainStepBetweenChangeDirectionMax");

        //Beach agents property
        beachStartHeight = serializedObject.FindProperty("beachMinimumHeight");
        beachEndHeight = serializedObject.FindProperty("beachMaximumHeight");
        beachHeightLimit = serializedObject.FindProperty("beachHeightLimit");
        beachWalkDistance = serializedObject.FindProperty("beachInlandWalkDistance");
        beachWalkSize = serializedObject.FindProperty("beachWalkSize");
        beachBrushRadius = serializedObject.FindProperty("beachBrushRadius");
        beachEnable = serializedObject.FindProperty("beachAgentEnable");

        //Smoothing agents property
        smoothEnable = serializedObject.FindProperty("smoothAgentEnable");
        smoothIterations = serializedObject.FindProperty("smoothIterations");
        smoothReturning = serializedObject.FindProperty("smoothReturning");

    }

    public override void OnInspectorGUI()
    {
        SoftwareAgentsGenerator generator = (SoftwareAgentsGenerator)target;

        
        serializedObject.Update();

        GeneralsParameters();
        AddBlankSeparator();

        CoastLineAgentsParameters();
        AddBlankSeparator();

        MountainAgentsParameters();
        AddBlankSeparator();

        BeachAgentsParameters();
        AddBlankSeparator();

        SmoothAgentsParameters();
        AddBlankSeparator();


        serializedObject.ApplyModifiedProperties();
    }

    public void BeachAgentsParameters()
    {
        AddProperty(beachEnable, "Enable beach agents");
        enableBeachAgents = beachEnable.boolValue;


        GUI.enabled = enableBeachAgents;

        showBeachAgents = EditorGUILayout.Foldout(showBeachAgents, "Beach agents settings");


        if (showBeachAgents)
        {
            AddProperty(beachHeightLimit, "Limit Height");
            AddProperty(beachStartHeight, "Minimum height");
            AddProperty(beachEndHeight, "Maximum height");
            AddMinMaxSlider(beachStartHeight, beachEndHeight, 0, 255);
            AddBlankSeparator();
            
            AddProperty(beachBrushRadius, "Brush Radius");
            AddProperty(beachWalkDistance, "Inland walk distance");
            AddProperty(beachWalkSize, "Inland walk steps");
            AddLineSeparator();
        }

        GUI.enabled = true;
    }

    public void MountainAgentsParameters()
    {
        AddProperty(mountainEnable, "Enable mountain agents");
        enableMountainAgents = mountainEnable.boolValue;

        GUI.enabled = enableMountainAgents;

        showMountainAgents = EditorGUILayout.Foldout(showMountainAgents, "Mountain agents settings");
        if (showMountainAgents)
        {
            AddProperty(mountainActiveAgents, "Agents count");
            AddBlankSeparator();

            AddProperty(mountainBottomWidthMin, "Minimum width of the base of the mountain");
            AddProperty(mountainBottomWidthMax, "Maximum width of the base of the mountain");
            AddMinMaxSlider(mountainBottomWidthMin, mountainBottomWidthMax, 5, 50);
            AddBlankSeparator();

            AddProperty(mountainTopWidthMin, "Minimum width of the top of the mountain");
            AddProperty(mountainTopWidthMax, "Maximum width of the top of the mountain");
            AddMinMaxSlider(mountainTopWidthMin, mountainTopWidthMax, 1, 25);
            AddBlankSeparator();

            AddProperty(mountainHeightMin, "Minimum height of the mountain");
            AddProperty(mountainHeightMax, "Maximum height of the mountain");
            AddMinMaxSlider(mountainHeightMin, mountainHeightMax, 10, 200);
            AddBlankSeparator();

            AddProperty(mountainStepBetweenChangeDirectionMin, "Minimum steps between the change of direction");
            AddProperty(mountainStepBetweenChangeDirectionMax, "Maximum steps between the change of direction");
            AddMinMaxSlider(mountainStepBetweenChangeDirectionMin, mountainStepBetweenChangeDirectionMax, 30, 200);
            AddLineSeparator();

        }
        GUI.enabled = true;
    }

    public void CoastLineAgentsParameters()
    {
        showCoastlineAgents = EditorGUILayout.Foldout(showCoastlineAgents, "Coastline agents settings");

        if (showCoastlineAgents)
        {
            AddProperty(coastlineStartPoint, "Start point");
            AddProperty(coastlineRadius, "Radius");
            AddBlankSeparator();
            AddProperty(coastlineWaterLevel, "Level of water");
            AddProperty(coastlineLandLevel, "Level of land");
            AddBlankSeparator();
            AddProperty(coastlineRemoveArtefacts, "Enable 'remove artefacts' step");
            AddLineSeparator();
        }
    }
    public void SmoothAgentsParameters()
    {
        AddProperty(smoothEnable, "Enable smoothing agents");
        enableSmoothAgents = smoothEnable.boolValue;

        GUI.enabled = enableSmoothAgents;

        showSmoothAgents = EditorGUILayout.Foldout(showSmoothAgents, "Smoothing agents settings");

        if(showSmoothAgents)
        {
            AddProperty(smoothIterations, "Iterations");
            AddProperty(smoothReturning, "Actions before return to start position");
            AddLineSeparator();
        }
        GUI.enabled = true;
    }

    public void GeneralsParameters()
    {
        AddProperty(generalSeed, "Seed");
        if (GUILayout.Button("Random seed"))
        {
            generalSeed.intValue = Random.Range(0, int.MaxValue);
        }
        AddBlankSeparator();

        AddProperty(generalTokens, "Tokens");
        AddProperty(generalTokensLimit, "Tokens limit");
        AddBlankSeparator();
    }
}
