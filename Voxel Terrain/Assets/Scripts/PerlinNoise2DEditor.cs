﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Perlin2DGenerator))]
[InitializeOnLoad]
public class PerlinNoise2DEditor : GeneratorEditor
{
    public float minValue = 0;
    float minValueLimit = 0;
    public float maxValue = 255;
    float maxValueLimit = 255;
    bool showPosition;
    bool caveEnable = false;


    static float rememberValueMin = -1;
    static float rememberValueMax = -1;


    SerializedProperty seed;
    SerializedProperty scaleX;
    SerializedProperty scaleY;
    SerializedProperty scaleZ;

    SerializedProperty octaves;
    SerializedProperty persistance;
    SerializedProperty lacunarity;

    SerializedProperty horizontalOffset;
    SerializedProperty verticalOffset;

    SerializedProperty cutoffMax;
    SerializedProperty cutoffMin;

    private void OnEnable()
    {
        seed = serializedObject.FindProperty("seed");
        scaleX = serializedObject.FindProperty("scaleX");
        scaleY = serializedObject.FindProperty("yAxisScale");
        scaleZ = serializedObject.FindProperty("scaleZ");


        octaves = serializedObject.FindProperty("octaves");
        persistance = serializedObject.FindProperty("persistance");
        lacunarity = serializedObject.FindProperty("lacunarity");

        horizontalOffset = serializedObject.FindProperty("horizontalOffset");
        verticalOffset = serializedObject.FindProperty("verticalOffset");

        cutoffMin = serializedObject.FindProperty("minCutOff");
        cutoffMax = serializedObject.FindProperty("maxCutOff");
    }

    public override void OnInspectorGUI()
    {
        Perlin2DGenerator generator = (Perlin2DGenerator)target;

        serializedObject.Update();

        AddProperty(seed, "Seed");
        if (GUILayout.Button("Random seed"))
        {
            generator.seed = Random.Range(0, int.MaxValue);
        }
        AddBlankSeparator();

        AddProperty(scaleX, "Scale x");
        AddProperty(scaleY, "Scale y");
        AddProperty(scaleZ, "Scale z");

        AddBlankSeparator();

        AddProperty(octaves, "Octaves");
        AddProperty(persistance, "Persistance");
        AddProperty(lacunarity, "Lacunarity");

        AddBlankSeparator();

        AddProperty(horizontalOffset, "Horizontal offset");
        AddProperty(verticalOffset, "Vertical offset");

        AddBlankSeparator();

        AddProperty(cutoffMin, "Cut off min");
        AddProperty(cutoffMax, "Cut off max");


        if (generator.minCutOff < (int)minValueLimit)
        {
            generator.minCutOff = (int)minValueLimit;
        }
        else if (generator.minCutOff > maxValueLimit)
        {
            generator.minCutOff = (int)maxValueLimit;
        }

        if (generator.maxCutOff > (int)maxValueLimit)
        {
            generator.maxCutOff = (int)maxValueLimit;
        }
        else if (generator.maxCutOff < (int)minValueLimit)
        {
            generator.maxCutOff = (int)minValueLimit;
        }

        if (generator.minCutOff > generator.maxCutOff)
        {
            generator.minCutOff = generator.maxCutOff;
        }

        minValue = (float)generator.minCutOff;
        maxValue = (float)generator.maxCutOff;

        EditorGUILayout.MinMaxSlider(ref minValue, ref maxValue, minValueLimit, maxValueLimit);

        cutoffMax.intValue = Mathf.CeilToInt(maxValue);
        cutoffMin.intValue = Mathf.CeilToInt(minValue);

        serializedObject.ApplyModifiedProperties();
    }

}
