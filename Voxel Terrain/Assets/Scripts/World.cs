﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
public class World : MonoBehaviour
{
    private List<Fragment> fragments = new List<Fragment>();


    


    public int numberOfFragmentsWidth;
    public int numberOfFragmentsDepth;

    public Material material;


    private Perlin2DGenerator perlin2dGenerator;
    private Perlin3DGenerator perlin3dGenerator;
    private SoftwareAgentsGenerator softwareAgentsGenerator;

    private long lastGeneratrionTime;
    private long SATime;
    private string lastSAImgFileName = string.Empty;

    delegate void WaitCallback(System.Object state);

    internal void SaveDataToFile()
    {
        string date = DateTime.Now.ToString("dd.MM.yyyy");
        string filename = "Data/" + date + ".txt";
        using (StreamWriter writer = new StreamWriter(filename, true))
        {
            writer.WriteLine(generator.ToString());
            writer.WriteLine(string.Format("numberOfFragmentsWidth: {0} numberOfFragmentsHeight: {1} MapSizeX {2} MapSizeY {3} MapSizeZ {4}", numberOfFragmentsWidth, numberOfFragmentsDepth, SizeX, SizeY, SizeZ));
            writer.WriteLine(selectedGenerator.GeneratorParameter());
            writer.WriteLine("Generation Time: " + lastGeneratrionTime + "ms");

            if (selectedGenerator is SoftwareAgentsGenerator)
            {
                writer.WriteLine("2D img " + lastSAImgFileName);
            }

            writer.WriteLine("###############################\r\n");
        }
    }

    internal void SaveDataToCsv()
    {
        string date = DateTime.Now.ToString("dd.MM.yyyy");
        string filename = "Data/" + date + ".csv";


        bool fileExist = File.Exists(filename);

        using (StreamWriter writer = new StreamWriter(filename, true))
        {
            if (!fileExist)
            {
                writer.WriteLine("Generowana wysokość ;czas generacji");
            }
            writer.WriteLine(string.Format("{0}", lastGeneratrionTime));
        }
            
    }




    public enum GeneratorType
    {
        Perlin2D,
        Perlin3D,
        SoftwareAgents
    }

    public GeneratorType generator;

    private Generator selectedGenerator = null;
    private MarchCubesRenderer selectedRenderer = null;

    private bool SaveToPNG(string filename)
    {
        Texture2D texture = new Texture2D(SizeX, SizeZ);

        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < SizeZ; y++)
            {
                //Color c = softwareAgentsGenerator.Map[x, y] == 0 ? Color.blue : new Color(softwareAgentsGenerator.Map[x, y] / 255f, softwareAgentsGenerator.Map[x, y] / 255f, softwareAgentsGenerator.Map[x, y] / 255f, 1.0f);
                Color c = softwareAgentsGenerator.Map[x, y] == 10 ? Color.blue : Color.green;
                texture.SetPixel(x, y, c);
            }
        }

        byte[] png = texture.EncodeToPNG();
        try
        {
            File.WriteAllBytes(filename, png);
            return true;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
    }

    private void SaveToCSV(string filename)
    {
        using (StreamWriter writer = new StreamWriter(filename))
        {


            for (int x = 0; x < SizeX; x++)
            {
                for (int y = 0; y < SizeZ; y++)
                {
                    writer.Write(softwareAgentsGenerator.Map[x, y]);
                    if (y != SizeZ - 1)
                    {
                        writer.Write(";");
                    }
                }
                writer.WriteLine();
            }
        }
    }

    public void Start()
    {
        UnityEngine.Random.seed = 1;

        perlin2dGenerator = GetComponent<Perlin2DGenerator>();
        perlin3dGenerator = GetComponent<Perlin3DGenerator>();
        softwareAgentsGenerator = GetComponent<SoftwareAgentsGenerator>();





        selectedRenderer = GetComponent<MarchCubesRenderer>();

        perlin2dGenerator.OnValidateGenerator += SelectedGenerator_OnValidateGenerator;
        perlin3dGenerator.OnValidateGenerator += SelectedGenerator_OnValidateGenerator;
        softwareAgentsGenerator.OnValidateGenerator += SelectedGenerator_OnValidateGenerator;

        Debug.Log("Metoda start klasy world");


        for (int i = 0; i < numberOfFragmentsDepth * numberOfFragmentsWidth; ++i)
        {
            GameObject gameObject = new GameObject("Fragment" + i);
            Fragment fragment = gameObject.AddComponent<Fragment>();
            fragment.world = this;
            fragment.MeshFiler = gameObject.AddComponent<MeshFilter>();
            fragment.MeshRenderer = gameObject.AddComponent<MeshRenderer>();
            fragments.Add(fragment);
        }


        for (int z = 0; z < numberOfFragmentsDepth; ++z)
        {
            for (int x = 0; x < numberOfFragmentsWidth; ++x)
            {
                int index = z * numberOfFragmentsDepth + x;

                Fragment fragment = fragments[index];

                fragment.fragmentPositionX = x;
                fragment.fragmentPositionZ = z;

                fragment.gameObject.transform.position = Vector3.Scale(new Vector3(x, 0, z), Fragment.FragmentSize / 2.0f);


                if (x > 0)
                {
                    fragment.leftNeighbor = fragments[index - 1];
                }
                if (x < numberOfFragmentsWidth - 1)
                {
                    fragment.rightNeighbor = fragments[index + 1];
                }
                if (z > 0)
                {
                    fragment.topNeighbor = fragments[index - numberOfFragmentsDepth];
                }
                if (z < numberOfFragmentsDepth - 1)
                {
                    fragment.bottomNeighbor = fragments[index + numberOfFragmentsDepth];
                }
            }
        }

        for (int i = 0; i < 10; ++i)
        {
            Refresh();
            SaveDataToCsv();
        }
        SaveDataToFile();
        Render();
        Refresh();

    }

    private void Render()
    {

        foreach (Fragment fragment in fragments)
        {
            if (selectedRenderer is MarchCubesRenderer)
            {

                fragment.MeshRenderer.sharedMaterial = this.material;
                selectedRenderer.Initialize();
                selectedRenderer.Render(fragment);
                fragment.MeshFiler.sharedMesh = selectedRenderer.ToMesh(fragment.MeshFiler.sharedMesh);
            }
            else
            {
                selectedRenderer.Render(fragment);
                //ThreadPool.QueueUserWorkItem(RenderFragment, fragment);
            }
        }
    }

    public void Generate()
    {
        switch (generator)
        {
            case GeneratorType.Perlin2D:
                selectedGenerator = perlin2dGenerator;
                break;
            case GeneratorType.Perlin3D:
                selectedGenerator = perlin3dGenerator;
                break;
            case GeneratorType.SoftwareAgents:
                selectedGenerator = softwareAgentsGenerator;
                break;

        }
        //ThreadPool.SetMaxThreads(fragments.Capacity, fragments.Capacity);


        if (selectedGenerator == softwareAgentsGenerator)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            softwareAgentsGenerator.Init();
            sw.Stop();
            SATime = sw.ElapsedMilliseconds;

        }

        Thread[] threads = new Thread[fragments.Count];
        for (int i = 0; i < fragments.Count; ++i)
        {
            threads[i] = new Thread(new ParameterizedThreadStart(GenerateFragment));
        }
        for (int i = 0; i < fragments.Count; ++i)
        {
            threads[i].Start(fragments[i]);
        }
        foreach (var t in threads)
        {
            t.Join();
        }


        //foreach (Fragment fragment in fragments)
        //{
        //    GenerateFragment(fragment);
        //}


        string directory = "Data/SA/";
        directory += DateTime.Now.ToString("dd.MM.yyyy");


        Directory.CreateDirectory(directory);

        string date = DateTime.Now.ToString("HH-mm-ss");

        //if (SaveToPNG(directory + "/" + date + ".png"))
        //{
        //    Debug.Log("Save png file");
        //    lastSAImgFileName = date + ".png";
        //}
    }



    private void GenerateFragment(object data)
    {
        Fragment fragment = data as Fragment;
        //Debug.Log("Generated fragment " + fragment.gameObject.name.ToString());
        selectedGenerator.GenerateFragment(fragment);
    }

    public void RenderFragment(object data)
    {
        Fragment fragment = data as Fragment;

        selectedRenderer.Render(fragment);


    }

    public void Refresh()
    {
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        Generate();
        sw.Stop();
        Debug.Log("Terrain generation time " + sw.ElapsedMilliseconds + " ms");
        lastGeneratrionTime = sw.ElapsedMilliseconds;
        TextureData colorTerrain = GetComponent<TextureData>();
        colorTerrain.savedMinHeight = 0;
        colorTerrain.savedMaxHeight = 255;
        colorTerrain.ApplyToMaterial(this.material);

        sw.Reset();
        sw.Start();
        //Render();
        sw.Stop();
        Debug.Log("Rendering time " + sw.ElapsedMilliseconds + " ms");
    }

    private void SelectedGenerator_OnValidateGenerator()
    {
    }


    public int SizeX
    {
        get
        {
            return (int)(numberOfFragmentsWidth * Fragment.FragmentSize.x);
        }
    }
    public int SizeY
    {
        get
        {
            return (int)Fragment.FragmentSize.y;
        }
    }
    public int SizeZ
    {
        get
        {
            return (int)(numberOfFragmentsDepth * Fragment.FragmentSize.z);
        }
    }
}