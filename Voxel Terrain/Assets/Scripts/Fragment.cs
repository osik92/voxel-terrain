﻿using UnityEngine;
public class Fragment : MonoBehaviour
{
    internal int fragmentPositionX = 0;
    internal int fragmentPositionZ = 0;


    internal MeshFilter MeshFiler;
    internal MeshRenderer MeshRenderer;


    internal Fragment leftNeighbor = null;
    internal Fragment rightNeighbor = null;
    internal Fragment topNeighbor = null;
    internal Fragment bottomNeighbor = null;

    internal float[] blocks;

    internal World world;


    public static readonly Vector3 FragmentSize = new Vector3(32, 256, 32);



    //private void OnDrawGizmosSelected()
    //{
    //    Vector3 blockSize = new Vector3(0.5f, 0.5f, 0.5f);



    //    for (int x = 0; x < Fragment.FragmentSize.x; ++x)
    //    {
    //        for (int y = 0; y < Fragment.FragmentSize.y; ++y)
    //        {
    //            for (int z = 0; z < Fragment.FragmentSize.z; ++z)
    //            {
    //                if(blocks[x,y,z] > 0)
    //                {
    //                    Gizmos.color = new Color(0, 1, 0, 1);
    //                    Vector3 position = transform.position + new Vector3(x * blockSize.x, y * blockSize.y, z * blockSize.z) + (blockSize / 2.0f);

    //                    Gizmos.DrawCube(position, blockSize);
    //                }
    //                else
    //                {
    //                    //Gizmos.color = new Color(0, 1, 0, 1);
    //                }


                    

    //            }
    //        }
    //    }
    //}
}
