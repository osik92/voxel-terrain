﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDemo : MonoBehaviour
{

    PerlinNoise noise = new PerlinNoise();
    float counter = 0;
    void Start()
    {

        Texture2D texture = new Texture2D(512, 512);

        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                float xx = x / 256.0f;
                float yy = y / 256.0f;

                texture.SetPixel(x, y, Color.Lerp(Color.white, Color.black, (float)noise.Perlin(xx,0, yy)));
            }
        }

        texture.Apply();
        MeshRenderer ren = GetComponent<MeshRenderer>();
        ren.sharedMaterial.mainTexture = texture;
    }




    void Update()
    {
        Texture2D texture = new Texture2D(512, 512);

        float val = counter / 256.0f;

        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                float xx = x / 256.0f;
                float yy = y / 256.0f;

                texture.SetPixel(x, y, Color.Lerp(Color.white, Color.black, (float)noise.Perlin(xx, val, yy)));
            }
        }

        texture.Apply();
        MeshRenderer ren = GetComponent<MeshRenderer>();
        ren.sharedMaterial.mainTexture = texture;
        counter++;
        
    }


    private void OnGUI()
    {
        float val = counter / 256.0f;
        GUI.Label(new Rect(0, 0, 100, 30), "val: " + val);
    }

}


