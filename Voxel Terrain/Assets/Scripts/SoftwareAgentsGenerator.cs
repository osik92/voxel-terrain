﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading;
using System.Text;

public partial class SoftwareAgentsGenerator : Generator
{

    public int generalSeed;
    public int generalTokensLimit;
    public int generalTokens;

    public Vector2 coastlineStartPoint;
    public int coastlineRadius;
    public int coastlineWaterLevel;
    public int coastlineLandLevel;
    public bool coastlineRemoveArtefacts;
    public bool coastlineShowCoastline;


    public int smoothReturning;
    public int smoothIterations;
    public bool smoothAgentEnable;


    public bool beachAgentEnable;
    public int beachMinimumHeight;
    public int beachMaximumHeight;
    public int beachHeightLimit;
    public int beachInlandWalkDistance;
    public int beachWalkSize;
    public int beachBrushRadius;

    public bool mountainAgentEnable;
    public int mountainBottomWidthMax;
    public int mountainBottomWidthMin;
    public int mountainTopWidthMax;
    public int mountainTopWidthMin;
    public int mountainHeightMax;
    public int mountainHeightMin;
    public int mountainStepBetweenChangeDirectionMax;
    public int mountainStepBetweenChangeDirectionMin;
    public int mountainAgentsCount;




    private World world;

    private List<Agent> agents;
    private System.Random random;
    private StringBuilder debugLog;




    private long timeCoastline;
    private long timeRemove;
    private long timeDetect;
    private long timeMountains;
    private long timeBeach;
    private long timeSmooth;


    private void OnValidate()
    {
        world = GetComponent<World>();
        GlobalSettings.Seed = this.generalSeed;
        GlobalSettings.Radious = this.coastlineRadius;
        GlobalSettings.Limit = this.generalTokensLimit;
        GlobalSettings.Tokens = this.generalTokens;
        GlobalSettings.SeaLevel = this.coastlineWaterLevel;
        GlobalSettings.Smooth = this.smoothReturning;
        GlobalSettings.StartPoint = this.coastlineStartPoint;
        GlobalSettings.LandLevel = this.coastlineLandLevel;
        GlobalSettings.BeachMinimumHeight = this.beachMinimumHeight;
        GlobalSettings.BeachMaximumHeight = this.beachMaximumHeight;
        GlobalSettings.BeachInlandWalkDistance = this.beachInlandWalkDistance;
        GlobalSettings.BeachInlandWalkSize = this.beachWalkSize;
        GlobalSettings.BeachBrushRadius = this.beachBrushRadius;
        GlobalSettings.BeachLimit = this.beachHeightLimit;

        GlobalSettings.MountainBottomWidthMax = mountainBottomWidthMax;
        GlobalSettings.MountainBottomWidthMin = mountainBottomWidthMin;
        GlobalSettings.MountainTopWidthMax = mountainTopWidthMax;
        GlobalSettings.MountainTopWidthMin = mountainTopWidthMin;
        GlobalSettings.MountainHeightMax = mountainHeightMax;
        GlobalSettings.MountainHeightMin = mountainHeightMin;
        GlobalSettings.MountainStepBetweenChangeDirectionMax = mountainStepBetweenChangeDirectionMax;
        GlobalSettings.MountainStepBetweenChangeDirectionMin = mountainStepBetweenChangeDirectionMin;
    }

    public float[,] Map
    {
        get { return GlobalSettings.map; }
    }

    public void Init()
    {
        random = new System.Random(generalSeed);
        debugLog = new StringBuilder();

        GlobalSettings.map = new float[world.SizeX, world.SizeZ];
        Vector2 startPoint = GlobalSettings.StartPoint;
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        Agent agent = new Agent(GlobalSettings.Tokens, world, startPoint, random.Next());


        agents = agent.DivideAgents();

        Thread[] threads = new Thread[agents.Count];
        for (int x = 0; x < GlobalSettings.map.GetLength(0); x++)
        {
            for (int y = 0; y < GlobalSettings.map.GetLength(1); y++)
            {
                GlobalSettings.map[x, y] = coastlineWaterLevel;
            }
        }


        #region CoastlineAgent
        sw.Start();
        for (int i = 0; i < agents.Count; i++)
        {
            agents[i] = new CoastlineAgent(agents[i]);
        }

        for (int i = 0; i < agents.Count; ++i)
        {
            threads[i] = new Thread(new ThreadStart(agents[i].Action));
        }
        for (int i = 0; i < agents.Count; ++i)
        {
            threads[i].Start(agents[i]);
        }
        foreach (var t in threads)
        {
            t.Join();
        }


        sw.Stop();
        timeCoastline = sw.ElapsedMilliseconds;
        Debug.LogFormat("[CoastlineAgent] time {0} ms", sw.ElapsedMilliseconds);
        debugLog.AppendFormat("[CoastlineAgent] time {0} ms", sw.ElapsedMilliseconds);

        if (coastlineRemoveArtefacts)
        {
            sw.Reset();
            sw.Start();
            RemoveArtefacts();
            sw.Stop();
            timeRemove = sw.ElapsedMilliseconds;
            Debug.LogFormat("[RemoveArtefacts] time {0} ms", sw.ElapsedMilliseconds);
            debugLog.AppendFormat("[RemoveArtefacts] time {0} ms", sw.ElapsedMilliseconds);
        }


        #endregion


        System.Diagnostics.Stopwatch swDS = new System.Diagnostics.Stopwatch();
        swDS.Start();
        DetectShoreline(startPoint);
        swDS.Stop();
        timeDetect = swDS.ElapsedMilliseconds;


        #region MountainAgent
        if (mountainAgentEnable)
        {

            sw.Reset();
            sw.Start();
            for (int it = 0; it < mountainAgentsCount; it++)
            {
                int i = it % agents.Count;
                agents[i] = new MountainAgent(agents[i], /*GlobalSettings.StartPoint ,*/random.NextVector2(new Vector2(world.SizeX, world.SizeZ) / 4, new Vector2(world.SizeX, world.SizeZ) - (new Vector2(world.SizeX, world.SizeZ) / 4)));
                agents[i].Action();

            }
            sw.Stop();
            timeMountains = sw.ElapsedMilliseconds;
            Debug.LogFormat("[MountainAgents] time {0} ms", sw.ElapsedMilliseconds);
            debugLog.AppendFormat("[MountainAgents] time {0} ms", sw.ElapsedMilliseconds);

        }
        #endregion

        #region BeachAgent
        if (beachAgentEnable)
        {
            if (GlobalSettings.Shoreline.Count > 0)
            {
                sw.Reset();
                sw.Start();

                threads = new Thread[agents.Count];
                for (int i = 0; i < agents.Count; i++)
                {
                    agents[i] = new BeachAgent(agents[i]);
                }

                for (int i = 0; i < agents.Count; ++i)
                {
                    threads[i] = new Thread(new ThreadStart(agents[i].Action));
                }
                for (int i = 0; i < agents.Count; ++i)
                {
                    threads[i].Start(agents[i]);
                }
                foreach (var t in threads)
                {
                    t.Join();
                }
                sw.Stop();
                timeBeach = sw.ElapsedMilliseconds;
                Debug.LogFormat("[BeachAgents] time {0} ms", sw.ElapsedMilliseconds);
                debugLog.AppendFormat("[BeachAgents] time {0} ms ", sw.ElapsedMilliseconds);
            }
            else
            {
                Debug.LogFormat("[BeachAgents] No detect Shoreline");
                debugLog.AppendFormat("[BeachAgents] No detect Shoreline ");
            }
        }
        #endregion

        #region SmoothAgent
        if (smoothAgentEnable)
        {
            System.Diagnostics.Stopwatch sw2 = new System.Diagnostics.Stopwatch();
			sw2.Start();
            for (int it = 0; it < smoothIterations; it++)
            {
                sw.Reset();
                sw.Start();



                for (int i = 0; i < agents.Count; i++)
                {
                    agents[i] = new SmoothAgent(agents[i], GlobalSettings.Smooth, random.NextVector2(new Vector2(world.SizeX, world.SizeZ)));
                }

                for (int i = 0; i < agents.Count; ++i)
                {
                    threads[i] = new Thread(new ThreadStart(agents[i].Action));
                }
                for (int i = 0; i < agents.Count; ++i)
                {
                    threads[i].Start(agents[i]);
                }
                foreach (var t in threads)
                {
                    t.Join();
                }

                sw.Stop();
                Debug.LogFormat("[SmoothAgent] iteration {0} time {1} ms", (it + 1), sw.ElapsedMilliseconds);
                debugLog.AppendFormat("[SmoothAgent] iteration {0} time {1} ms ", (it + 1), sw.ElapsedMilliseconds);
            }
            sw2.Stop();
            timeSmooth = sw2.ElapsedMilliseconds;
            Debug.LogFormat("[SmoothAgents] time {0} ms", sw2.ElapsedMilliseconds);
            debugLog.AppendFormat("\r\n[SmoothAgents] time {0} ms ", sw2.ElapsedMilliseconds);

        }
        #endregion
    }


    public void OnDrawGizmos()
    {
        if (coastlineShowCoastline)
        {
            Vector3 blockSize = new Vector3(0.5f, 0.5f, 0.5f);
            foreach (var shoreline in GlobalSettings.Shoreline)
            {
                float height = GlobalSettings.map[(int)shoreline.position.x, (int)shoreline.position.y];
                Vector3 position = new Vector3(shoreline.position.x * blockSize.x, height * blockSize.y, shoreline.position.y * blockSize.z) + (blockSize / 2.0f);
                Gizmos.color = new Color(0, 1, 0);
                Gizmos.DrawCube(position, blockSize);
            }
        }
    }


    private void DetectShoreline(Vector2 startPoint)
    {
        MapElement[] tempMap = new MapElement[world.SizeX * world.SizeZ];
        Queue<MapElement> queue = new Queue<MapElement>();
        // Prepare tempMap;

        startPoint = startPoint.Clamp(Vector2.zero, new Vector2(world.SizeX - 1, world.SizeZ - 1));


        for (int z = 0; z < world.SizeZ; z++)
        {
            for (int x = 0; x < world.SizeX; x++)
            {
                tempMap[z * world.SizeX + x] = new MapElement()
                {
                    isCoastline = false,
                    position = new Vector2(x, z),
                    type = MapElement.MapElementType.notVisited
                };
            }
        }

        var startElement = tempMap[(int)(startPoint.y * world.SizeX + startPoint.x)];
        startElement.type = MapElement.MapElementType.active;
        queue.Enqueue(startElement);

        Vector2[] directions = new Vector2[]
        {
            new Vector2(0, -1),
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(-1, 0)
        };

        while (queue.Count > 0)
        {
            var currentMapElement = queue.Dequeue();

            for (int i = 0; i < directions.Length; i++)
            {
                var newElementPosition = currentMapElement.position + directions[i];

                if (newElementPosition.x > 0 && newElementPosition.x < world.SizeX &&
                   newElementPosition.y > 0 && newElementPosition.y < world.SizeZ)
                {
                    var newMapElement = tempMap[(int)(newElementPosition.y * world.SizeX + newElementPosition.x)];
                    if (newMapElement.type == MapElement.MapElementType.notVisited && GlobalSettings.map[(int)newElementPosition.x, (int)newElementPosition.y] > coastlineWaterLevel)
                    {
                        newMapElement.type = MapElement.MapElementType.active;
                        queue.Enqueue(newMapElement);
                    }
                    if (GlobalSettings.map[(int)newElementPosition.x, (int)newElementPosition.y] <= coastlineWaterLevel)
                    {
                        currentMapElement.isCoastline = true;
                    }

                }

            }
            currentMapElement.type = MapElement.MapElementType.visited;
        }
        GlobalSettings.Shoreline = tempMap.Where(sl => sl.isCoastline == true).ToList();
    }

    private Vector2 GetNearestShorelinePoint(Vector2 location)
    {
        //return GlobalSettings.Shoreline.Min<float>( x => ((x.position.x - location.x) * (x.position.x - location.x)) + ((x.position.y - location.y) * (x.position.y - location.y)));
        return GlobalSettings.Shoreline.Aggregate((minItem, nextItem) => Vector2.Distance(location, minItem.position) < Vector2.Distance(location, nextItem.position) ? minItem : nextItem).position;

    }


    private void RemoveArtefacts()
    {
        Vector2[] directions = new Vector2[]
            {
                new Vector2(-1, 0),
                new Vector2(-1, -1),
                new Vector2(0, -1),
                new Vector2(1, -1),
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(0, 1),
                new Vector2(-1, 1),
                new Vector2(0, 0)
            };


        for (int x = 0; x < world.SizeX; ++x)
        {
            for (int z = 0; z < world.SizeZ; ++z)
            {
                float height = GlobalSettings.map[x, z];// * 3;
                float avgHeight = 0;
                int avgDivider = 0;
                int blocks = 9;
                int water = 0;
                for (int i = 0; i < directions.Length; i++)
                {
                    Vector2 pos = new Vector2(x, z) + directions[i];
                    if (!pos.InRange(Vector2.zero, new Vector2(world.SizeX, world.SizeZ)))
                    {
                        blocks--;
                    }

                    else if (GlobalSettings.map[(int)pos.x, (int)pos.y] <= coastlineWaterLevel)
                    {
                        water++;
                    }
                    else
                    {
                        avgHeight += GlobalSettings.map[(int)pos.x, (int)pos.y];
                        avgDivider++;
                    }
                }



                if (water >= blocks / 2f)
                {
                    GlobalSettings.map[x, z] = coastlineWaterLevel;
                }
                else if (height <= coastlineWaterLevel)
                {
                    height = avgHeight / (float)avgDivider;
                }
                else
                {
                    GlobalSettings.map[x, z] = height;
                }
            }

        }


    }

    private List<Vector2> GetPointsAroundLocation(Vector2 location, int areaRadious)
    {
        HashSet<Vector2> points = new HashSet<Vector2>();


        var startPos = new Vector2(location.x, location.y - areaRadious);
        for (int i = 0; i <= areaRadious; i++)
        {
            //draw line
            for (int j = 0; j <= i; j++)
            {
                var pos1 = startPos + new Vector2(-j, i);

                if (Extension.InRange(pos1, Vector2.zero, new Vector2(world.SizeX, world.SizeZ)))
                {
                    points.Add(pos1);
                }

                var pos2 = startPos + new Vector2(j, i);
                if (Extension.InRange(pos2, Vector2.zero, new Vector2(world.SizeX, world.SizeZ)))
                {
                    points.Add(pos2);
                }
            }
        }
        startPos = location;
        for (int i = 1; i <= areaRadious; i++)
        {
            //draw line
            for (int j = 0; j <= areaRadious - i; j++)
            {
                var pos1 = startPos + new Vector2(-j, i);
                if (Extension.InRange(pos1, Vector2.zero, new Vector2(world.SizeX, world.SizeZ)))
                {
                    points.Add(pos1);
                }
                var pos2 = startPos + new Vector2(j, i);
                if (Extension.InRange(pos2, Vector2.zero, new Vector2(world.SizeX, world.SizeZ)))
                {
                    points.Add(pos2);
                }
            }
        }
        return points.ToList();
    }



    public override void GenerateFragment(Fragment fragment)
    {
        fragment.blocks = new float[(int)(Fragment.FragmentSize.x * Fragment.FragmentSize.y * Fragment.FragmentSize.z)];


        for (int z = 0; z < Fragment.FragmentSize.z; z++)
        {
            for (int x = 0; x < Fragment.FragmentSize.x; x++)
            {

                int samlpeX = (int)((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x);
                int sampleZ = (int)((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z);

                float height = GlobalSettings.map[samlpeX, sampleZ];
                for (int y = 0; y < Fragment.FragmentSize.y; y++)
                {
                    if (y > height)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = 1;
                    }
                    else if (y == height)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = 0;
                    }
                    else
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = -1;
                    }

                }
            }

        }
    }

    public override string GeneratorParameter()
    {
        debugLog.AppendLine(string.Format("\r\nseed: {0}, tokens: {1}, tokens limit: {2}", generalSeed, generalTokens, generalTokensLimit));
        debugLog.AppendLine("=== COASTLINE AGENTS ===");
        debugLog.AppendLine(string.Format("start point: (x: {0}, z:{1}),  radius: {2}, water level: {3}, land level: {4}, remove artefacts step: {5}", coastlineStartPoint.x, coastlineStartPoint.y, coastlineRadius, coastlineWaterLevel, coastlineLandLevel, coastlineRemoveArtefacts ? "enabled" : "disabled"));
        if (mountainAgentEnable)
        {
            debugLog.AppendLine("=== MOUNTAIN AGENTS ===");
            debugLog.AppendLine(string.Format("bottom width: ({0} - {1}), top width: ({2} - {3}), height: ({4} - {5}), steps between change direction: ({6} - {7}), active agents: {8}", mountainBottomWidthMin, mountainBottomWidthMax, mountainTopWidthMin, mountainTopWidthMax, mountainHeightMin, mountainHeightMax, mountainStepBetweenChangeDirectionMin, mountainStepBetweenChangeDirectionMax, mountainAgentsCount));
        }
        if (beachAgentEnable)
        {
            debugLog.AppendLine("=== BEACH AGENTS ===");
            debugLog.AppendLine(string.Format("height: ({0} - {1}), height limit: {2}, inland walk distance: {3}, inland walk steps: {4}, brush radius: {5}", beachMinimumHeight, beachMaximumHeight, beachHeightLimit, beachInlandWalkDistance, beachWalkSize, beachBrushRadius));
        }
        if (smoothAgentEnable)
        {
            debugLog.AppendLine("=== SMOOTHING AGENTS ===");
            debugLog.AppendLine(string.Format("action before returning: {0}, smooth iterations: {1}", smoothReturning, smoothIterations));
        }

        return debugLog.ToString();
    }


    public string GetTimesCSV()
    {
        return string.Format("{0};{1};{2};{3};{4};{5}", timeCoastline, timeRemove, timeDetect, timeMountains, timeBeach, timeSmooth);
    }

    public class MapElement
    {
        public enum MapElementType
        {
            notVisited,
            active,
            visited
        }
        public MapElementType type;
        public bool isCoastline;
        public Vector2 position;
    }

    class Agent
    {
        private int tokens;
        private int startTokens;
        private World world;
        Vector2 startPosition;
        protected System.Random random;
        private int seed;
        private SoftwareAgentsGenerator generator;

        public Agent(int tokens, World world, Vector2 startPosition, int seed)
        {
            this.tokens = tokens;
            this.startTokens = tokens;
            this.world = world;
            this.startPosition = startPosition;
            this.seed = seed;
            random = new System.Random(seed);
        }
        public int Tokens
        {
            get { return this.tokens; }
            protected set { this.tokens = value; }
        }

        public int StartTokens
        {
            get { return this.startTokens; }
        }
        public World World
        {
            get
            {
                return this.world;
            }
        }

        public Vector2 StartPosition
        {
            get { return this.startPosition; }
        }

        public int Seed
        {
            get
            {
                return this.seed;
            }
        }

        public List<Agent> DivideAgents()
        {
            List<Agent> agents = Divide();
            agents.RemoveAll(a => a.Tokens <= 0);
            return agents;
        }

        private List<Agent> Divide()
        {
            List<Agent> agents = new List<Agent>();
            agents.Add(this);

            if (this.Tokens >= GlobalSettings.Limit)
            {
                Agent child1, child2;


                Vector2 pos = random.NextUnitCircle();

                pos.Scale(random.NextVector2(new Vector2(GlobalSettings.Radious, GlobalSettings.Radious)));
                pos = Vector2.ClampMagnitude(pos, GlobalSettings.Radious);
                pos += startPosition;
                pos = pos.Clamp(Vector2.zero, new Vector2(world.SizeX, world.SizeZ));

                child1 = new Agent(this.Tokens / 2, this.world, pos, random.Next());

                pos = random.NextUnitCircle();

                pos.Scale(random.NextVector2(new Vector2(GlobalSettings.Radious, GlobalSettings.Radious)));
                pos = Vector2.ClampMagnitude(pos, GlobalSettings.Radious);
                pos += startPosition;
                pos = pos.Clamp(Vector2.zero, new Vector2(world.SizeX - 1, world.SizeZ - 1));

                child2 = new Agent(this.Tokens / 2, this.world, pos, random.Next());
                Tokens -= child1.Tokens;
                Tokens -= child2.Tokens;
                agents.AddRange(child1.Divide());
                agents.AddRange(child2.Divide());
            }

            return agents;
        }
        public virtual void Action() { }
    }

    class SmoothAgent : Agent
    {
        int actionsBeforeReturnToStartPoint;
        int actions;
        public SmoothAgent(Agent agent, int actionsBeforeReturnToStartPoint) : this(agent, actionsBeforeReturnToStartPoint, agent.StartPosition)
        {
        }

        public SmoothAgent(Agent agent, int actionsBeforeReturnToStartPoint, Vector2 startPosition) : base(agent.StartTokens, agent.World, startPosition, agent.Seed)
        {
            this.actions = this.actionsBeforeReturnToStartPoint = actionsBeforeReturnToStartPoint;
        }

        public override void Action()
        {
            Vector2 location = StartPosition;

            Vector2[] directions = new Vector2[]
            {
                new Vector2(-2,0),
                new Vector2(-1, 0),
                new Vector2(0, -1),
                new Vector2(0, -2),
                new Vector2(1, 0),
                new Vector2(2, 0),
                new Vector2(0, 1),
                new Vector2(0, 2)
            };

            while (Tokens > 0)
            {
                Tokens--;

                float height = GlobalSettings.map[(int)location.x, (int)location.y] * 3;


                for (int i = 0; i < directions.Length; i++)
                {
                    var pos = location + directions[i];
                    pos = pos.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
                    height += GlobalSettings.map[(int)pos.x, (int)pos.y];
                }


                GlobalSettings.map[(int)location.x, (int)location.y] = height / 11f;

                if (actions <= 0)
                {
                    actions = actionsBeforeReturnToStartPoint;
                    location = StartPosition;
                }
                else
                {
                    actions--;
                    Vector2 pos = Vector2.zero;
                    do
                    {
                        Vector2[] direction = new Vector2[]
                        {
                            new Vector2(-1,0),
                            new Vector2(-1,1),
                            new Vector2(0,1),
                            new Vector2(1,1),
                            new Vector2(1,0),
                            new Vector2(1,-1),
                            new Vector2(0,-1),
                            new Vector2(-1,-1)
                        };


                        pos = location + direction[random.Next(direction.Length)];

                        pos = pos.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
                    } while (pos == location);
                    location = pos;

                }

            }
        }



    }

    class BeachAgent : Agent
    {
        public BeachAgent(Agent agent) : base(agent.StartTokens, agent.World, agent.StartPosition, agent.Seed)
        {
            /*
             1 - odnalezienie punktów wybrzerza
             2 - Wylosowanie punktu startowego na lini brzegowej
             3 - 
             */
        }

        public override void Action()
        {
            Vector2 location = GlobalSettings.Shoreline[random.Next(GlobalSettings.Shoreline.Count)].position; //StartPosition;

            Vector2[] neighbors = new Vector2[]
            {
                new Vector2(-1,0),
                new Vector2(-1,-1),
                new Vector2(0,-1),
                new Vector2(1,-1),
                new Vector2(1,0),
                new Vector2(1,1),
                new Vector2(0,1),
                new Vector2(-1,1),
            };

            while (Tokens > 0)
            {

                if (GlobalSettings.map[(int)location.x, (int)location.y] >= GlobalSettings.BeachLimit)
                {
                    Tokens--;
                    continue;
                }
                var points = GetPointsAroundLocation(location, GlobalSettings.BeachBrushRadius);

                //FlattenArea(points, GlobalSettings.MinimumBeachHeight, GlobalSettings.MaximumBeachHeight /2);
                FlattenArea(points, GlobalSettings.BeachMinimumHeight, GlobalSettings.BeachMinimumHeight + (GlobalSettings.BeachMaximumHeight - GlobalSettings.BeachMinimumHeight) / 2);
                //SmoothArea(points);


                var startPoint = GlobalSettings.StartPoint.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
                Vector2 inland = startPoint - location;
                inland.Normalize();
                int distance = random.Next(GlobalSettings.BeachInlandWalkDistance / 2, GlobalSettings.BeachInlandWalkDistance);

                inland.Scale(new Vector2(distance, distance));
                inland += location;



                for (int i = 0; i < GlobalSettings.BeachInlandWalkSize; ++i)
                {
                    if (GlobalSettings.map[(int)inland.x, (int)inland.y] >= GlobalSettings.BeachLimit)
                    {
                        continue;
                    }
                    points = GetPointsAroundLocation(inland, GlobalSettings.BeachBrushRadius);
                    FlattenArea(points, GlobalSettings.BeachMinimumHeight + (GlobalSettings.BeachMaximumHeight - GlobalSettings.BeachMinimumHeight) / 2, GlobalSettings.BeachMaximumHeight);
                    //SmoothArea(points);

                    inland = inland + neighbors[random.Next(neighbors.Length)];
                }
                location = GlobalSettings.Shoreline[random.Next(GlobalSettings.Shoreline.Count)].position;
                //location = location + neighbors[random.Next(neighbors.Length)];
                Tokens--;
            }
        }

        private void SmoothArea(List<Vector2> points)
        {
            Vector2[] directions = new Vector2[]
            {
                new Vector2(-2,0),
                new Vector2(-1, 0),
                new Vector2(0, -1),
                new Vector2(0, -2),
                new Vector2(1, 0),
                new Vector2(2, 0),
                new Vector2(0, 1),
                new Vector2(0, 2)
            };
            foreach (Vector2 point in points)
            {
                float height = GlobalSettings.map[(int)point.x, (int)point.y] * 3;


                for (int i = 0; i < directions.Length; i++)
                {
                    var pos = point + directions[i];
                    pos = pos.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
                    height += GlobalSettings.map[(int)pos.x, (int)pos.y];
                }


                GlobalSettings.map[(int)point.x, (int)point.y] = height / 11f;
            }

        }

        private void FlattenArea(List<Vector2> points, int min, int max)
        {
            int flatHeight = random.Next(min, max);
            foreach (Vector2 point in points)
            {
                GlobalSettings.map[(int)point.x, (int)point.y] = flatHeight;
            }
        }

        private List<Vector2> GetPointsAroundLocation(Vector2 location, int areaRadious)
        {
            HashSet<Vector2> points = new HashSet<Vector2>();


            var startPos = new Vector2(location.x, location.y - areaRadious);
            for (int i = 0; i <= areaRadious; i++)
            {
                //draw line
                for (int j = 0; j <= i; j++)
                {
                    var pos1 = startPos + new Vector2(-j, i);

                    if (Extension.InRange(pos1, Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        points.Add(pos1);
                    }

                    var pos2 = startPos + new Vector2(j, i);
                    if (Extension.InRange(pos2, Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        points.Add(pos2);
                    }
                }
            }
            startPos = location;
            for (int i = 1; i <= areaRadious; i++)
            {
                //draw line
                for (int j = 0; j <= areaRadious - i; j++)
                {
                    var pos1 = startPos + new Vector2(-j, i);
                    if (Extension.InRange(pos1, Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        points.Add(pos1);
                    }
                    var pos2 = startPos + new Vector2(j, i);
                    if (Extension.InRange(pos2, Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        points.Add(pos2);
                    }
                }
            }
            return points.ToList();
        }
    }

    class MountainAgent : Agent
    {

        int mountainHeight;
        int mountainTopWidth = 2;
        int mountainBottomWidth = 20;
        int stepBetweenChangeDirection;
        Vector2 TravelDirection;
        int gap = 25;

        public MountainAgent(Agent agent) : this(agent, agent.StartPosition) { }
        public MountainAgent(Agent agent, Vector2 startPosition) : base(agent.StartTokens, agent.World, startPosition, agent.Seed)
        {

            Debug.LogWarning("MountainAgent seed" + Seed);
            float xDirection = ((random.Next(int.MaxValue) / (float)int.MaxValue) * 2f) - 1f;
            float yDirection = ((random.Next(int.MaxValue) / (float)int.MaxValue) * 2f) - 1f;
            Vector2 direction = new Vector2(xDirection, yDirection);
            TravelDirection = direction.normalized;


            Debug.Log(Seed.ToString() + " xDir: " + xDirection + " yDir: " + yDirection);
            mountainHeight = random.Next(GlobalSettings.MountainHeightMin, GlobalSettings.MountainHeightMax);
            mountainBottomWidth = random.Next(GlobalSettings.MountainBottomWidthMin, GlobalSettings.MountainBottomWidthMax);
            mountainTopWidth = random.Next(GlobalSettings.MountainTopWidthMin, GlobalSettings.MountainTopWidthMax);
            stepBetweenChangeDirection = random.Next(GlobalSettings.MountainStepBetweenChangeDirectionMin, GlobalSettings.MountainStepBetweenChangeDirectionMax);
        }

        public override void Action()
        {
            Vector2 location = StartPosition;

            Vector2 direction = /*Vector2.left;*/TravelDirection;
            gap = 25;
            while (GlobalSettings.map[(int)location.x, (int)location.y] <= GlobalSettings.SeaLevel)
            {
                location = new Vector2(random.Next(World.SizeX), random.Next(World.SizeZ));
                gap--;
                if (gap <= 0)
                {
                    Debug.Log("[MountainAgent] Cannot find startPositon ("+ StartPosition.ToString()+"), break action");
                    return;
                }
            }
            gap = 25;

            int angle = (int)(Vector2.Dot(direction, Vector2.up) * Mathf.Rad2Deg);
            int angleplus = angle + 45;
            int angleminus = angle - 45;

            while (Tokens > 0)
            {
                gap = 25;
                while (!location.InRange(Vector2.zero, new Vector2(World.SizeX, World.SizeZ)) || GlobalSettings.map[(int)location.x, (int)location.y] <= GlobalSettings.SeaLevel)
                {
                    angle = random.Next(angleminus, angleplus);
                    direction = new Vector2(Mathf.Sin(angle * Mathf.Deg2Rad), Mathf.Cos(angle * Mathf.Deg2Rad)).normalized;
                    location += direction;
                    gap--;
                    if (gap <= 0)
                    {
                        Debug.Log("[MountainAgent] Too many collision, break action");
                        return;
                    }
                }


                ElevateWedge(location, direction);
                SmoothArea(location, direction);
                SmoothArea(location, direction);
                SmoothArea(location, direction);

                if (Tokens % stepBetweenChangeDirection == 0)
                {
                    angle = random.Next(angleminus, angleplus);
                    direction = new Vector2(Mathf.Sin(angle * Mathf.Deg2Rad), Mathf.Cos(angle * Mathf.Deg2Rad)).normalized;
                }
                location += direction;
                Tokens--;
            }

            ////while(Tokens > 0)
            ////{
            ////    Tokens--;
            ////    ElevateWedge(location, direction);
            ////    // Podnieś na daną wysokość odpowiedni obszar prostopadle to kierunku
            ////    // Wygładzić obszar wokół aktualnej pozycji
            ////    // zmienić pozycje na kolejny punkt wzdłóż kierunku


            ////    if(Tokens % 100/*Co ile tokenów należy zmienić kierunek*/ == 0)
            ////    {
            ////        // zmiana kierunku o +- 45 stopni względem oryginalnego kierunku
            ////    }
            ////}

        }

        private void SmoothArea(Vector2 location, Vector2 direction)
        {
            int mountainBottomWidth = 20;


            Vector2[] directions = new Vector2[]
            {
                new Vector2(-2,0),
                new Vector2(-1, 0),
                new Vector2(0, -1),
                new Vector2(0, -2),
                new Vector2(1, 0),
                new Vector2(2, 0),
                new Vector2(0, 1),
                new Vector2(0, 2)
            };
            Vector2[] shape = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(-1, 0),
                new Vector2(0,-1),
                new Vector2(1, 0),
                new Vector2(0, 1),
            };

            for (int i = 0; i <= (mountainBottomWidth / 2); ++i)
            {
                for (int k = 0; k < shape.Length; k++)
                {
                    var position = location + (direction.Ortogonal() * i) + shape[k];
                    if (!position.InRange(Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        continue;
                    }

                    float height = GlobalSettings.map[(int)position.x, (int)position.y] * 3;
                    for (int j = 0; j < directions.Length; j++)
                    {
                        var pos = position + directions[j];
                        pos = pos.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
                        height += GlobalSettings.map[(int)pos.x, (int)pos.y];
                    }
                    GlobalSettings.map[(int)position.x, (int)position.y] = height / 11;
                    if (i == 0)
                        continue;
                    position = location - (direction.Ortogonal() * i) + shape[k];
                    if (!position.InRange(Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        continue;
                    }
                    height = GlobalSettings.map[(int)position.x, (int)position.y] * 3;
                    for (int j = 0; j < directions.Length; j++)
                    {
                        var pos = position + directions[j];
                        pos = pos.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
                        height += GlobalSettings.map[(int)pos.x, (int)pos.y];
                    }
                    GlobalSettings.map[(int)position.x, (int)position.y] = height / 11;
                }
            }
        }

        private void ElevateWedge(Vector2 location, Vector2 direction)
        {
            //założenie wstępne
            /*
             Podnosimy o 100 jednostek w górę
             Góra ma szerokość przy podstawie 50, przy wierzchołku 5
             Operujemy na kolumnach

             */

            Vector2[] shape = new Vector2[]
            {
                new Vector2(-1, 0),
                new Vector2(0,-1),
                new Vector2(1, 0),
                new Vector2(0, 1),
                new Vector2(0, 0),
            };




            float heightRange = (mountainBottomWidth - mountainTopWidth) / 2f;

            for (int i = -(mountainBottomWidth / 2); i <= mountainBottomWidth / 2; ++i)
            {
                for (int j = 0; j < shape.Length; j++)
                {
                    var position = location + (direction.Ortogonal().normalized * i) + shape[j];

                    if (!position.InRange(Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        continue;
                    }
                    if (Math.Abs(i) <= mountainTopWidth / 2)
                        GlobalSettings.map[(int)position.x, (int)position.y] = mountainHeight + GlobalSettings.LandLevel;
                    else
                    {
                        GlobalSettings.map[(int)position.x, (int)position.y] = (mountainHeight / heightRange) * ((mountainBottomWidth / 2f) - Math.Abs(i) + (mountainTopWidth / 2f)) + GlobalSettings.LandLevel;
                    }
                }

            }

        }
    }

    public class GlobalSettings
    {
        public static float[,] map;

        // Coastline Agents
        public static int Radious;
        public static int Limit;
        public static int SeaLevel;
        public static int Tokens;


        public static Vector2 StartPoint;
        // Smoth Agents
        public static int Smooth;

        //Beach Agents
        public static float BeachLimit;
        public static float AreaRadious;
        public static int BeachMinimumHeight;
        public static int BeachMaximumHeight;

        // Mountain Agents

        public static List<MapElement> Shoreline = new List<MapElement>();
        internal static int LandLevel;
        internal static int BeachInlandWalkDistance;
        internal static int BeachBrushRadius;
        internal static int MountainBottomWidthMin;
        internal static int MountainBottomWidthMax;
        internal static int MountainTopWidthMax;
        internal static int MountainTopWidthMin;
        internal static int MountainHeightMax;
        internal static int MountainHeightMin;
        internal static int MountainStepBetweenChangeDirectionMax;
        internal static int MountainStepBetweenChangeDirectionMin;
        internal static int Seed;

        public static int BeachInlandWalkSize { get; internal set; }

        public static string ToString()
        {
            return string.Format("{0}_{1}_{2}_{3}_{4}", Seed, Radious, Limit, Tokens, Smooth);
        }
    }
}

public static class Extension
{
    public static Vector2 Clamp(this Vector2 vector, Vector2 min, Vector2 max)
    {
        return new Vector2(Mathf.Clamp(vector.x, min.x, max.x), Mathf.Clamp(vector.y, min.y, max.y));
    }

    public static bool InRange(this Vector2 value, Vector2 min, Vector2 max)
    {
        return value.x >= min.x && value.y >= min.y && value.x < max.x && value.y < max.y;
    }

    public static Vector2 Ortogonal(this Vector2 vector)
    {
        return new Vector2(-vector.y, vector.x);
    }

    public static Vector2 NextVector2(this System.Random random, Vector2 min, Vector2 max)
    {
        return new Vector2(random.NextFloat(min.x, max.x), random.NextFloat(min.y, max.y));
    }

    public static Vector2 NextVector2(this System.Random random, Vector2 max)
    {
        return random.NextVector2(Vector2.zero, max);
    }

    public static float NextFloat(this System.Random random, float min, float max)
    {
        float rand = (float)(random.Next()) / (float)(int.MaxValue);
        float diff = max - min;
        float r = rand * diff;
        return min + r;
    }
    public static float NextFloat(this System.Random random, float max)
    {
        return random.NextFloat(0f, max);
    }

    public static Vector2 NextUnitCircle(this System.Random random)
    {
        float angle = random.NextFloat(2 * Mathf.PI);
        return new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)).normalized;
    }
}
