﻿using System;
using UnityEngine;

public class Perlin2DGenerator : Generator
{
    public int seed;

    [Range(100, 2000)]
    public float scaleX = 300;

    [Range(100, 2000)]
    public float scaleZ = 300;

    [Range(1, 32)]
    public int octaves;
    public float persistance;
    public float lacunarity;
    public Vector2 horizontalOffset;
    [Range(-255, 255)]
    public int verticalOffset = 0;

    [Range(0.5f, 5.0f)]
    public float yAxisScale = 1.0f;


    public int maxCutOff = 255;
    public int minCutOff = 0;

    

    public override void GenerateFragment(Fragment fragment)
    {
        fragment.blocks = new float[32*256*32];

        System.Random random = new System.Random(this.seed);
        Vector2[] octaveOffsets = new Vector2[this.octaves];

        for (int i = 0; i < this.octaves; i++)
        {
            float offsetX = random.Next(10000) + this.horizontalOffset.x;
            float offsetY = random.Next(10000) + this.horizontalOffset.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);
        }

        if (this.scaleX <= 0)
        {
            this.scaleX = 0.0001f;
        }
        if (this.scaleZ <= 0)
        {
            this.scaleZ = 0.0001f;
        }


        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        
        for (int z = 0; z < Fragment.FragmentSize.z; z++)
        {
            for (int x = 0; x < Fragment.FragmentSize.x; x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;
                
                
                // float samlpeX = ((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x + this.Offset.x);
                // float sampleZ = ((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z + this.Offset.y);
                
                // float elevation = Mathf.PerlinNoise(samlpeX/elevationScale, sampleZ/elevationScale) * 2 - 1;
                // float roughness = Mathf.PerlinNoise(samlpeX/roughnessScale, sampleZ/roughnessScale) * 2 - 1;
                // float detail = Mathf.PerlinNoise(samlpeX/detailScale, sampleZ/detailScale) * 2 - 1;

                // noiseHeight = (elevation + (roughness * detail)) * 128 + 128;
                

                for (int i = 0; i < this.octaves; i++)
                {
                float samlpeX = ((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x + octaveOffsets[i].x) / this.scaleX * frequency;
                float sampleZ = ((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z + octaveOffsets[i].y) / this.scaleZ * frequency;

                    float perlinValue = Mathf.PerlinNoise(samlpeX, sampleZ) * 2 - 1;
                    noiseHeight += perlinValue * amplitude * yAxisScale;

                amplitude *= persistance;
                frequency *= lacunarity;

                }

                //float fHeight = noiseHeight;

                float fHeight = Map(noiseHeight, -1, 1, 0, 255);
                int height = (int)Mathf.Clamp(fHeight + verticalOffset, 2, 254);

                float decimalPlace = (float)(fHeight - Math.Truncate(fHeight));


                //Debug.Log(decimalPlace);
                for (int y = 0; y < Fragment.FragmentSize.y; y++)
                {

                    //fragment.blocks[x, height, z] = noiseHeight;

                    if (y <= minCutOff)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = -1;// - val;
                    }
                    else if (y >= maxCutOff)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = 1;
                    }else if(y >= height)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = Mathf.Abs(decimalPlace + decimalPlace);// * (y-height);
                    }
                    else if (y == height)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = decimalPlace;
                    }
                    else if (y == height - 1)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = 1 - decimalPlace;
                    }
                    else
                    {
                        var val = decimalPlace * (height - y);
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = - Mathf.Abs(decimalPlace);// - val;
                    }
                }

            }
        }



        //for (int x = 0; x < Fragment.FragmentSize.x; ++x)
        //{
        //    for (int y = 0; y < Fragment.FragmentSize.y; ++y)
        //    {
        //        for (int z = 0; z < Fragment.FragmentSize.z; ++z)
        //        {
        //            fragment.blocks[x, y, z] = ((float)random.NextDouble() * 2.0f) - 1.0f;
        //        }
        //    }
        //}
        //throw new NotImplementedException();
    }

    private float Map(float value, float iStart, float iStop, float oStart, float oStop)
    {
        return oStart + (oStop - oStart) * ((value - iStart) / (iStop - iStart));
    }

    public override string GeneratorParameter()
    {
        return string.Format("seed: {0}, scale: (x: {1}, y: {11}, z:{2}),  octaves: {3}, persistance {4}, lacunarity {5}, horizontal offset (x: {6}, z: {7}), vertical offset: {8}, cut off: ({9} - {10}))", seed, scaleX, scaleZ, octaves, persistance, lacunarity, horizontalOffset.x, horizontalOffset.y, verticalOffset, minCutOff, maxCutOff, yAxisScale);
    }


}
