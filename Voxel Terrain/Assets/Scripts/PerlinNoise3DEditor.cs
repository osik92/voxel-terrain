﻿using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(Perlin3DGenerator))]
//[InitializeOnLoad]
public class PerlinNoise3DEditor : GeneratorEditor
{

    public float minValue = 0;
    float minValueLimit = 0;
    public float maxValue = 255;
    float maxValueLimit = 255;

    SerializedProperty seed;
    SerializedProperty scaleX;
    SerializedProperty scaleY;
    SerializedProperty scaleZ;
    SerializedProperty heightInfluance;

    SerializedProperty octaves;
    SerializedProperty persistance;
    SerializedProperty lacunarity;

    SerializedProperty horizontalOffset;
    SerializedProperty verticalOffset;

    SerializedProperty cutoffMax;
    SerializedProperty cutoffMin;

    private void OnEnable()
    {
        seed = serializedObject.FindProperty("seed");
        scaleX = serializedObject.FindProperty("xAxsicScale");
        scaleY = serializedObject.FindProperty("yAxsicScale");
        scaleZ = serializedObject.FindProperty("zAxsicScale");
        heightInfluance = serializedObject.FindProperty("heightInfluance");

        octaves = serializedObject.FindProperty("octaves");
        persistance = serializedObject.FindProperty("persistance");
        lacunarity = serializedObject.FindProperty("lacunarity");

        horizontalOffset = serializedObject.FindProperty("horizontalOffset");
        verticalOffset = serializedObject.FindProperty("verticalOffset");

        cutoffMax = serializedObject.FindProperty("maxCutOff");
        cutoffMin = serializedObject.FindProperty("minCutOff");
    }


    public override void OnInspectorGUI()
    {
        Perlin3DGenerator generator = (Perlin3DGenerator)target;


        serializedObject.Update();

        AddProperty(seed, "Seed");
        if (GUILayout.Button("Random seed"))
        {
            generator.seed = Random.Range(0, int.MaxValue);
        }
        AddBlankSeparator();

        AddProperty(scaleX, "Scale x");
        AddProperty(scaleY, "Scale y");
        AddProperty(scaleZ, "Scale z");
        AddProperty(heightInfluance, "Height influance");

        AddBlankSeparator();

        AddProperty(octaves, "Octaves");
        AddProperty(persistance, "Persistance");
        AddProperty(lacunarity, "Lacunarity");

        AddBlankSeparator();

        AddProperty(horizontalOffset, "Horizontal offset");
        AddProperty(verticalOffset, "Vertical offset");

        AddBlankSeparator();

        AddProperty(cutoffMin, "Cut off min");
        AddProperty(cutoffMax, "Cut off max");


        


        if (generator.minCutOff < (int)minValueLimit)
        {
            generator.minCutOff = (int)minValueLimit;
        }
        else if (generator.minCutOff > maxValueLimit)
        {
            generator.minCutOff = (int)maxValueLimit;
        }

        if (generator.maxCutOff > (int)maxValueLimit)
        {
            generator.maxCutOff = (int)maxValueLimit;
        }
        else if (generator.maxCutOff < (int)minValueLimit)
        {
            generator.maxCutOff = (int)minValueLimit;
        }

        if (generator.minCutOff > generator.maxCutOff)
        {
            generator.minCutOff = generator.maxCutOff;
        }

        minValue = (float)generator.minCutOff;
        maxValue = (float)generator.maxCutOff;

        EditorGUILayout.MinMaxSlider(ref minValue, ref maxValue, minValueLimit, maxValueLimit);

        cutoffMax.intValue = Mathf.CeilToInt(maxValue);
        cutoffMin.intValue = Mathf.CeilToInt(minValue);


        serializedObject.ApplyModifiedProperties();
    }

}
