﻿using System;
using System.IO;

public class SphereGenerator : Generator
{
    public override void GenerateFragment(Fragment fragment)
    {
        StreamWriter file = new StreamWriter("test.txt", true);

        fragment.blocks = new float[32*256*32];
        for (int z = 0; z < Fragment.FragmentSize.z; z++)
        {
            for (int y = 0; y < Fragment.FragmentSize.y; y++)
            {

                for (int x = 0; x < Fragment.FragmentSize.x; x++)
                {
                    var xx = Fragment.FragmentSize.x / 2 - x;
                    var yy = Fragment.FragmentSize.y / 2 - y;
                    var zz = Fragment.FragmentSize.z / 2 - z;

                    var value = (xx * xx) + (yy * yy) + (zz * zz) - 200.0f;

                    fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = value;
                    file.Write(value + " ");


                }
                file.WriteLine();
            }
            file.WriteLine();
        }

        file.Close();
    }

    public override string GeneratorParameter()
    {
        return string.Empty;
    }
}