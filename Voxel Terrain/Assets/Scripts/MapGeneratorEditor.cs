﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(World), true)]
public class MapGeneratorEditor : GeneratorEditor
{

    SerializedProperty fragmentXAxis;
    SerializedProperty fragmentZAxis;
    SerializedProperty generationMethod;

    private void OnEnable()
    {
        fragmentXAxis = serializedObject.FindProperty("numberOfFragmentsWidth");
        fragmentZAxis = serializedObject.FindProperty("numberOfFragmentsDepth");
        generationMethod = serializedObject.FindProperty("generator");
    }

    public override void OnInspectorGUI()
    {
        World mapGen = (World)target;


        AddProperty(fragmentXAxis, "Number of fragments width");
        AddProperty(fragmentXAxis, "Number of fragments depth");
        AddProperty(generationMethod, "Generation method");

        AddBlankSeparator();

        if (GUILayout.Button("Regenerate terrain"))
        {
            mapGen.Refresh();
        }

        if (GUILayout.Button("Save terrain data to file"))
        {
            mapGen.SaveDataToFile();
        }
    }


}
