﻿using System.Collections.Generic;
using UnityEngine;
public class GizmosRenderer : Renderer
{
    private List<Vector3> drawableCubes = new List<Vector3>();
    private Vector3 blockSize = new Vector3(0.5f, 0.5f, 0.5f);
    public override void Render(Fragment fragment)
    {
        

        for (int x = 0; x < Fragment.FragmentSize.x; ++x)
        {
            for (int y = 0; y < Fragment.FragmentSize.y; ++y)
            {
                for (int z = 0; z < Fragment.FragmentSize.z; ++z)
                {
                    if(fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z +  y * (int)Fragment.FragmentSize.z + z] > 0)
                    {
                        Vector3 position = fragment.transform.position + new Vector3(x * blockSize.x, y * blockSize.y, z * blockSize.z) + (blockSize / 2.0f);
                        drawableCubes.Add(position);
                    }
                }
            }
        }
    }

    private bool BlockIsVisible(Fragment fragment, int x, int y, int z)
    {
        bool isVisible = false;

        for (int xx = x -1 ; xx < x +1 ; ++xx)
        {
            for (int yy = x - 1; yy < y + 1; ++yy)
            {
                for (int zz = x - 1; zz < z + 1; ++zz)
                {
                    if(xx == x && yy == y && zz == z)
                    {
                        continue;
                    }


                    if (xx < 0 || xx >= Fragment.FragmentSize.x ||
                        yy < 0 || yy >= Fragment.FragmentSize.y ||
                        zz < 0 || zz >= Fragment.FragmentSize.z)
                    {
                        if (fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] > 0)
                        {
                            isVisible = true;
                            break;
                        }
                        
                    }
                    else
                    {
                        if(fragment.blocks[xx * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + yy * (int)Fragment.FragmentSize.z + zz] < 0 && fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z +  y * (int)Fragment.FragmentSize.z + z] > 0)
                        {
                            isVisible = true;
                            break;
                        }
                    }
                }
            }
        }

        return isVisible;
    }

    private void OnDrawGizmos()
    {
        foreach (Vector3 position in drawableCubes)
        {
            Gizmos.color = new Color(0, 1, 0);
            Gizmos.DrawCube(position, blockSize);
        }
    }
}