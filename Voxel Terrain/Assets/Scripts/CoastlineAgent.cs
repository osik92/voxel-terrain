﻿using UnityEngine;

public partial class SoftwareAgentsGenerator
{
    class CoastlineAgent : Agent
    {
        Vector2 Direction;
        Vector2 AttractorPosition;
        Vector2 RepulsorPosition;

        private const int MaxGapInTraveling = 1;

        public CoastlineAgent(Agent agent) : base(agent.StartTokens, agent.World, agent.StartPosition, agent.Seed)
        {
            Direction = random.NextUnitCircle();
            AttractorPosition = random.NextVector2(new Vector2(World.SizeX - 1, World.SizeZ - 1));
            RepulsorPosition = random.NextVector2(new Vector2(World.SizeX - 1, World.SizeZ - 1));
        }

        public override void Action()
        {
            int landLevel = GlobalSettings.LandLevel;// random.Next(GlobalSettings.LandLevel, GlobalSettings.LandLevel + 20);
            int gapInTraveling = MaxGapInTraveling;

            Vector2 pos = RandomNextPoint(this.StartPosition);
            while (Tokens > 0)
            {
                float score = float.MinValue;
                Vector2 coords = pos;
                Vector2 bestCoors = coords;
                for (int x = -1; x <= 1; x++)
                {
                    for (int z = -1; z <= 1; z++)
                    {
                        coords = pos + new Vector2(x, z);
                        coords = coords.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
                        if (GlobalSettings.map[(int)coords.x, (int)coords.y] != GlobalSettings.SeaLevel /*|| (x==0 && z==0)*/)
                        {
                            continue;
                        }
                        float val = Score(coords);
                        if (val > score)
                        {
                            bestCoors = coords;
                            score = val;
                        }
                    }
                }

                if(score == float.MinValue)
                {
                    gapInTraveling--;
                    pos += Direction;

                    //Debug.Log("Point is surrounded");
                    if (gapInTraveling < 0 || !pos.InRange(Vector2.zero, new Vector2(World.SizeX, World.SizeZ)))
                    {
                        //Debug.Log("Most then " + MaxGapInTraveling + " gaps");
                        gapInTraveling = MaxGapInTraveling;
                        
                        Tokens--;

                        pos = RandomNextPoint(this.StartPosition);
                        continue;
                    }
                    continue;
                }
                GlobalSettings.map[(int)bestCoors.x, (int)bestCoors.y] = landLevel;
                pos = RandomNextPoint(this.StartPosition);

                Tokens--;
            }
        }

        private float Score(Vector2 coords)
        {
            return Vector2.Distance(coords, RepulsorPosition) - Vector2.Distance(coords, AttractorPosition) + 3 * MinDistanceToEdge(coords);
        }

        private float MinDistanceToEdge(Vector2 coords)
        {
            float minDist = float.MaxValue;

            Vector2[] neerestEdge = new Vector2[]
            {
                new Vector2(coords.x, -1),
                new Vector2(coords.x, World.SizeZ),
                new Vector2(-1, coords.y),
                new Vector2(World.SizeX, coords.y)
            };

            for (int i = 0; i < neerestEdge.Length; i++)
            {
                var dist = Vector2.Distance(coords, neerestEdge[i]);
                if (dist < minDist)
                {
                    minDist = dist;
                }
            }

            return minDist;
        }

        private Vector2 RandomNextPoint(Vector2 seedPoint)
        {
            Vector2 pos = random.NextUnitCircle();
            pos.Scale(random.NextVector2(new Vector2(GlobalSettings.Radious, GlobalSettings.Radious)));
            pos = Vector2.ClampMagnitude(pos, GlobalSettings.Radious);
            pos += seedPoint;
            pos = pos.Clamp(Vector2.zero, new Vector2(World.SizeX - 1, World.SizeZ - 1));
            return pos;
        }
    }
}


