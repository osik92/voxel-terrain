﻿using System;

public class RandomGenerator : Generator
{
    private System.Random random = new System.Random();
    private float[,,] fullRandomNoise;
    private const int NoiseSize = 16;
    public RandomGenerator()
    {
        fullRandomNoise = new float[NoiseSize, NoiseSize, NoiseSize];
        for (int x = 0; x < fullRandomNoise.GetLength(0); x++)
        {
            for (int y = 0; y < fullRandomNoise.GetLength(1); y++)
            {
                for (int z = 0; z < fullRandomNoise.GetLength(2); z++)
                {
                    fullRandomNoise[x,y,z] = ((float)random.NextDouble() * 2.0f) - 1.0f;
                }
            }
                
        }
    }

    public override void GenerateFragment(Fragment fragment)
    {
        fragment.blocks = new float[32*256*32];
        for (int x = 0; x < Fragment.FragmentSize.x; ++x)
        {
            for (int y = 0; y < Fragment.FragmentSize.y; ++y)
            {
                for (int z = 0; z < Fragment.FragmentSize.z; ++z)
                {
                    var zz = z - Fragment.FragmentSize.z / 2;
                    var yy = y - Fragment.FragmentSize.y / 2;
                    var xx = x - Fragment.FragmentSize.x / 2;
                    float density = yy * 2;//


                    density += fullRandomNoise[(x * 4) % NoiseSize, (y) % NoiseSize, (z * 4) % NoiseSize] * 1f;
                    density += fullRandomNoise[(x * 2) % NoiseSize, (y) % NoiseSize, (z * 2) % NoiseSize] * 3.0f;
                    density += fullRandomNoise[(x * 1) % NoiseSize, (y * 1) % NoiseSize, (z * 1) % NoiseSize] * 0.3f;


                    fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = density;
                }
            }
        }
    }

    public override string GeneratorParameter()
    {
        return string.Empty;
    }
}
