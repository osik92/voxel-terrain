﻿using System;
using UnityEngine;
public abstract class Generator : MonoBehaviour
{
    public abstract void GenerateFragment(Fragment fragment);
    public abstract string GeneratorParameter();

    public delegate void OnValidateHandler();
    internal event OnValidateHandler OnValidateGenerator;

    private void OnValidate()
    {
        if (OnValidateGenerator != null)
        {
            OnValidateGenerator();
        }
    }
}
