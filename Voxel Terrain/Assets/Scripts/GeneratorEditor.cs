﻿using UnityEditor;
using UnityEngine;
public abstract class GeneratorEditor : Editor
{
    public void AddProperty(SerializedProperty property, string labelText)
    {
        if (labelText == null)
            labelText = property.name;

        EditorGUILayout.PropertyField(property, new GUIContent() { text = labelText });
    }

    public void AddProperty(SerializedProperty property)
    {
        this.AddProperty(property, null);
    }

    public void AddMinMaxSlider(SerializedProperty min, SerializedProperty max, int minLimit, int maxLimit)
    {
        float minValue = min.intValue;
        float maxValue = max.intValue;

        EditorGUILayout.MinMaxSlider(ref minValue, ref maxValue, minLimit, maxLimit);
        min.intValue = (int)minValue;
        max.intValue = (int)maxValue;

    }

    public void AddMinMaxSlider(SerializedProperty min, SerializedProperty max, float minLimit, float maxLimit)
    {
        this.AddMinMaxSlider(min, max, (int)minLimit, (int)maxLimit);
    }

    public void AddBlankSeparator()
    {
        EditorGUILayout.LabelField("");
    }

    public void AddLineSeparator()
    {
        AddBlankSeparator();
        GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
    }
}
