﻿using System;
using System.IO;
using UnityEngine;

public class Perlin3DGenerator : Generator
{

    //[Range(10f, 100f)]
    //public float horizontalScale = 100;
    //[Range(0.1f, 10.0f)]
    //public float verticalScale = 1;
    //[Range(1.0f, 10.0f)]
    //public float groundLevelScale = 10;
    //[Range(1.0f, 10.0f)]
    //public float heightOffset = 5;
    //public float groundLevelOffset = 5;
    //[Range(0.1f, 10.0f)]
    //public float offset = 3;
    //[Range(0.1f, 1.0f)]
    //public float persistance;
    //[Range(1, 10)]
    //public int octaves;
    //[Range(0, 254)]
    //public int groundLevel;

    public Vector2 horizontalOffset;


    #region New Parammeters

    public int seed;


    public int minCutOff = 0;
    public int maxCutOff = 255;

    [Range(-255, 255)]
    public int verticalOffset = 0;
       

    [Range(1, 32)]
    public int octaves = 1;

    public float persistance = 2.0f;
    public float lacunarity = 0.5f;

    [Range(0.0001f, 1)]
    public float xAxsicScale = 0.15f;

    [Range(0.5f, 5)]
    public float yAxsicScale = 2.5f;

    [Range(0.0001f, 1)]
    public float zAxsicScale = 0.15f;


    [Range(0.5f, 2.0f)]
    public float heightInfluance = 0.80f;

    #endregion
    public override void GenerateFragment(Fragment fragment)
    {
        PerlinNoise noise = new PerlinNoise();
        fragment.blocks = new float[(int)(Fragment.FragmentSize.x * Fragment.FragmentSize.y * Fragment.FragmentSize.z)];

        #region disable

        //double[,] dim = new double[,]
        //{
        //    {1.0, -1.0, -0.05 },
        //    {-1.0, 1.0, 0.05 },
        //    {-1.0, 1.0, 0.05 }
        //};

        //int[] res = new int[3];
        //for (var i = 0; i < 3; ++i)
        //{
        //    res[i] = 2 + (int)System.Math.Ceiling((dim[i, 1] - dim[i, 0]) / dim[i, 2]);
        //}


        //var x = dim[0, 0] - dim[0, 2];
        //for (var i = 0; i < res[0]; ++i, x += dim[0, 2])
        //{
        //    var y = dim[1, 0] - dim[1, 2];
        //    for (var j = 0; j < res[1]; ++j, y += dim[1, 2])
        //    {
        //        var z = dim[2, 0] - dim[2, 2];
        //        for (var k = 0; k < res[2]; ++k, z += dim[2, 2])
        //        {
        //            var value = noise.Perlin(x * 2.0 + 5, y * 2 + 3.0, z * 2 + 0.6);
        //            fragment.blocks[i * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + j * (int)Fragment.FragmentSize.z + k] = (float) (y + value);
        //        }
        //    }
        //}

        #endregion

        System.Random random = new System.Random(seed);

        Vector3[] octaveOffsets = new Vector3[this.octaves];
        double noiseHeight = 0;

        for (int i = 0; i < octaves; ++i)
        {
            float offsetX = random.Next(-10000, 10000) ;
            float offsetY = random.Next(-10000, 10000) + this.verticalOffset;
            float offsetZ = random.Next(-10000, 10000) ;
            octaveOffsets[i] = new Vector3(offsetX, offsetY, offsetZ);
        }


        for (int z = 0; z < Fragment.FragmentSize.z; z++)
        {
            for (int x = 0; x < Fragment.FragmentSize.x; x++)
            {
                int igrek = 0;
                for (int y = 0, sampleStep = 0; y < Fragment.FragmentSize.y; y++, igrek++)
                {

                    if (y <= minCutOff)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = -1;
                        continue;
                    }
                    else if (y >= maxCutOff)
                    {
                        fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = 1;
                        continue;
                    }

                    var yy = (y + verticalOffset) / 256.0f;
                    
                        double total = 0;
                        double frequency = 1;
                        double amplitude = 1;
                        double maxValue = 0;            // Used for normalizing result to 0.0 - 1.0
                        for (int i = 0; i < octaves; i++)
                        {
                            var xx = ((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x + this.horizontalOffset.x) / 32.0f * frequency * xAxsicScale + octaveOffsets[i].x;
                            var zz = ((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z + this.horizontalOffset.y) / 32.0f * frequency * zAxsicScale + octaveOffsets[i].z;

                            total += noise.Perlin(xx  , yy * yAxsicScale * frequency, zz) * amplitude;

                            maxValue += amplitude;

                            amplitude *= persistance;
                            frequency *= lacunarity;
                        }

                        noiseHeight = 0.3f + ((heightInfluance * yy) - (total /maxValue));




                    //noiseHeight = GetValue3(fragment, x, y, z);

                    fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = (float)noiseHeight;
                }
            }
        }




        //for (int z = 0; z < Fragment.FragmentSize.z; z++)
        //{

        //    for (int x = 0; x < Fragment.FragmentSize.x; x++)
        //    {

        //        float v1 = 0, v2 = 0;
        //        int igrek = 0;
        //        for (int y = 0, sampleStep = 0; y < Fragment.FragmentSize.y; y++, igrek++)
        //        {

        //            //var zz = z - Fragment.FragmentSize.z / 2;

        //            //var xx = x - Fragment.FragmentSize.x / 2;
        //            //zz /= 32;

        //            //xx /= 32;
        //            //float samlpeX = ((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x) / horizontalScale;

        //            //float sampleZ = ((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z) / horizontalScale;
        //            //if (y % 8 == 0)
        //            //{
        //            //    float sampleY = sampleStep * verticalScale;
        //            //    var yy = y - Fragment.FragmentSize.y / 2;
        //            //    yy /= 256;
        //            //    v1 = (yy) * groundLevelScale + offset + (float)noise.OctavePerlin(samlpeX + horizontalOffset.x, sampleY + heightOffset, sampleZ + horizontalOffset.y, octaves, persistance);

        //            //    sampleStep++;
        //            //    sampleY = sampleStep * verticalScale;
        //            //    yy = y + 8 - Fragment.FragmentSize.y / 2;
        //            //    yy /= 256;
        //            //    v2 = (yy) * groundLevelScale + offset + (float)noise.OctavePerlin(samlpeX + horizontalOffset.x, sampleY + heightOffset, sampleZ + horizontalOffset.y, octaves, persistance);

        //            //    igrek = 0;
        //            //}

        //            //if (y == 0)
        //            //{
        //            //    fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = -1;
        //            //    continue;
        //            //}
        //            //else if (y == Fragment.FragmentSize.y - 1)
        //            //{
        //            //    fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = 1;
        //            //    continue;
        //            //}

        //            float height = GetValue3(fragment, x, y, z);

        //            //float height = GetValue(fragment, x, y, z);
        //            //float height = Mathf.Lerp(v1, v2, (0.125f * igrek));
        //            fragment.blocks[x * (int)Fragment.FragmentSize.y * (int)Fragment.FragmentSize.z + y * (int)Fragment.FragmentSize.z + z] = height;


        //        }
        //    }
        //}
    }


    private float GetValue(Fragment fragment, int x, int y, int z)
    {
        //PerlinNoise noise = new PerlinNoise();
        //if (y == 0)
        //{
        //    return -1;
        //}
        //else if (y == Fragment.FragmentSize.y - 1)
        //{
        //    return 1;
        //}
        //var zz = z - Fragment.FragmentSize.z / 2;
        //var yy = y - Fragment.FragmentSize.y / 2;
        //var xx = x - Fragment.FragmentSize.x / 2;
        //zz /= 32;
        //yy /= 256;
        //xx /= 32;

        //float samlpeX = ((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x) / horizontalScale;
        //float sampleY = yy * verticalScale;
        //float sampleZ = ((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z) / horizontalScale;

        //return yy * groundLevelScale + offset + (float)noise.OctavePerlin(samlpeX + horizontalOffset.x, sampleY + heightOffset, sampleZ + horizontalOffset.y, octaves, persistance);
        return 0;
    }

    private float GetValue2(Fragment fragment, int x, int y, int z)
    {
        //PerlinNoise noise = new PerlinNoise();
        //if (y == 0)
        //{
        //    return -1;
        //}
        //else if (y == Fragment.FragmentSize.y - 1)
        //{
        //    return 1;
        //}

        //var zz = z - Fragment.FragmentSize.z / 2;
        //var yy = y - Fragment.FragmentSize.y / 2;
        //var xx = x - Fragment.FragmentSize.x / 2;
        //zz /= 32;
        //yy /= 256;
        //xx /= 32;

        //if (y % 8 == 0)
        //{

        //}


        //float samlpeX = ((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x) / horizontalScale;
        //float sampleY = yy * verticalScale;
        //float sampleZ = ((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z) / horizontalScale;

        //return (yy) * groundLevelScale + offset + (float)noise.OctavePerlin(samlpeX + horizontalOffset.x, sampleY + heightOffset, sampleZ + horizontalOffset.y, octaves, persistance);
        return 0;
    }

    float GenerateValueToSaveToFile(Fragment fragment, int x, int y, int z)
    {
        PerlinNoise noise = new PerlinNoise();
        var zz = z - Fragment.FragmentSize.z / 2; // change positive value to range -value/2 - value/2
        var yy = y - Fragment.FragmentSize.y / 2; // change positive value to range -value/2 - value/2
        var xx = x - Fragment.FragmentSize.x / 2; // change positive value to range -value/2 - value/2
        zz /= 16; // change to range -1  +1
        yy /= 128; // change to range -1  +1
        xx /= 16; // change to range -1  +1

        return yy + (float)noise.Perlin(xx * 2 + 5, yy * 2 + 3, zz * 2 + 0.6f);
    }

    private float GetValue3(Fragment fragment, int x, int y, int z)
    {
        PerlinNoise noise = new PerlinNoise();
        if (y <= minCutOff)
        {
            return -1;
        }
        else if (y >= maxCutOff)
        {
            return 1;
        }

        var xx = ((fragment.fragmentPositionX * Fragment.FragmentSize.x) + x + horizontalOffset.x) / 32.0f;
        var yy = (y + verticalOffset) / 256.0f;
        var zz = ((fragment.fragmentPositionZ * Fragment.FragmentSize.z) + z + horizontalOffset.y) / 32.0f;

        //return (float)noise.Perlin(xx*0.5, 0.1 * y, zz*0.5);
        return 0.3f + (float)((heightInfluance * yy) - noise.OctavePerlin(xx * xAxsicScale, yy * yAxsicScale, zz * zAxsicScale, octaves, persistance, lacunarity));

    }


    public override string GeneratorParameter()
    {
        return string.Format("seed: {0}, cut off: ({1} - {2}), octaves: {3}, persistance: {4}, lacunarity: {5}, scale: (x:{6} y:{7} z:{8}), heightinfluance: {9}, horizontal offset: (x: {10}, z: {11}), vertical offset: {12}", seed, minCutOff, maxCutOff, octaves, persistance, lacunarity, xAxsicScale, yAxsicScale, zAxsicScale, heightInfluance, horizontalOffset.x, horizontalOffset.y, verticalOffset);

    }

}
