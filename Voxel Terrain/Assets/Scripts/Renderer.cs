﻿using System;
using UnityEngine;
public abstract class Renderer : MonoBehaviour
{
    public abstract void Render(Fragment fragment);

}
