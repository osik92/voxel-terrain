﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class TextureData : MonoBehaviour {

    internal float savedMinHeight;
    internal float savedMaxHeight;

    const int textureSize = 512;
    const TextureFormat textureFormat = TextureFormat.RGB565;

    public Layer[] layers;
    
   
    internal void ApplyToMaterial(Material material)
    {
        material.SetInt("layerCount", layers.Length);
        material.SetColorArray("baseColours", layers.Select(x=>x.tint).ToArray() );
        material.SetFloatArray("baseStartHeights", layers.Select(x => x.Height).ToArray());
        material.SetFloatArray("baseBlends", layers.Select(x => x.blendStrength).ToArray());
        material.SetFloatArray("baseColoursStrength", layers.Select(x => x.tintStrength).ToArray());
        material.SetFloatArray("baseTextureScale", layers.Select(x => x.textureScale).ToArray());
        Texture2DArray texturesArray = GenerateTextureArray(layers.Select(x => x.texture).ToArray());
        material.SetTexture("baseTextures", texturesArray);
        

        UpdateMeshHeights(material);
    }

    private Texture2DArray GenerateTextureArray(Texture2D[] textures)
    {
        Texture2DArray textureArray = new Texture2DArray(textureSize, textureSize, textures.Length, textureFormat, true);
        for (int i = 0; i < textures.Length; i++)
        {
            textureArray.SetPixels(textures[i].GetPixels(), i);
        }
        textureArray.Apply();
        return textureArray;
    }

    private void UpdateMeshHeights(Material material)
    {
        material.SetFloat("minHeight", 0);
        material.SetFloat("maxHeight", 255);
    }

    private void OnValidate()
    {
        World world = GetComponent<World>();
        ApplyToMaterial(world.material);
    }

    [System.Serializable]
    public class Layer
    {
        public Texture2D texture;
        public Color tint;
        [Range(0, 1)]
        public float tintStrength;
        [Range(0, 255)]
        public int startHeight;
        [Range(0, 1)]
        public float blendStrength;
        public float textureScale;

        internal float Height
        {
            get { return startHeight / 2.0f; }
        }
    }
}

